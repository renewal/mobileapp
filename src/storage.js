// Global Redux store for the App, with persistence
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { AsyncStorage } from 'react-native';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';

import { rootReducer } from './actions';
import { loggerStateTransformer } from './utils';

const middleware = getDefaultMiddleware({
  serializableCheck: {
    // redux-persist uses some special actions that are non-serializable,
    // so we want to ignore those in the serializable check
    ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE', 'persist/PURGE']
  },
  // the default 32ms for warnAfter to is too short for when the list of
  // articles becomes large (though this might be useful to help us determine
  // a max size for the articles list)
  immutableCheck: { warnAfter: 200 }
});

if (__DEV__) {
  middleware.push(
    createLogger({
      stateTransformer: loggerStateTransformer
    })
  );
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware,
  devTools: __DEV__
});

const persistedStore = persistStore(store);

export { store, persistConfig };
export default persistedStore;
