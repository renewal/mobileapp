/* Polyfill for Intl support */
import 'intl';
import 'intl/locale-data/jsonp/en';
import 'intl/locale-data/jsonp/fr';

import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AppLoading, SplashScreen } from 'expo';
import { Asset } from 'expo-asset';
import Constants from 'expo-constants';
import * as Font from 'expo-font';
import {
  getTrackingPermissionsAsync,
  isAvailable
} from 'expo-tracking-transparency';
import { Root } from 'native-base';
import Roboto from 'native-base/Fonts/Roboto.ttf';
import RobotoMedium from 'native-base/Fonts/Roboto_medium.ttf';
import React, { Component } from 'react';
import {
  Button,
  ImageBackground,
  Platform,
  View,
  StyleSheet
} from 'react-native';
import { Provider, connect } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// Font for the masthead
import Chomsky from '../assets/Chomsky.otf';
import accountActions from './actions/account';
import AppTracking from './components/AppTracking';
import TickMessage from './components/TickMessage';
import ArticleView, { ArticleViewHeader } from './containers/ArticleView';
import Home from './containers/Home';
import persistedStore, { store } from './storage';
import { sleep } from './utils';

if (__DEV__) {
  const config = Constants.manifest;
  console.log(`Loaded config file from ${config.extra.environmentConfig}`);
  console.log(`Config: ${JSON.stringify(config)}`);
}

const styles = StyleSheet.create({
  splashScreen: {
    width: '100%',
    height: '100%',
    alignItems: 'center'
  },
  splashMessage: {
    position: 'absolute',
    bottom: 20,
    alignItems: 'center'
  }
});

const Stack = createStackNavigator();

class _RootContainer extends Component {
  state = {
    isReady: false,
    splashMessage: '',
    permissionsGranted: false
  };

  componentDidMount() {
    this._loadAsync().then(
      () => this._checkPermissions()
    );
  }

  _clearState() {
    persistedStore.purge();
    store.dispatch({ type: 'RESET' });
  }

  render() {
    let splashMessage = this.state.splashMessage;
    if (!splashMessage.length && this.props.isAuthenticating) {
      // If there is no other message to display on the splash screen but we
      // are still waiting on authentication, display the authentication message
      splashMessage = 'Logging in';
    }

    const {permissionsGranted} = this.state;

    // Display splash screen
    if (!this.state.isReady) {
      // See https://docs.expo.io/versions/latest/sdk/splash-screen/#example-without-any-flickering-between-splashscreen-and
      return (
        <Root>
          <ImageBackground
            style={styles.splashScreen}
            source={require('../assets/splash.png')}
            resizeMode="contain"
            onLoadEnd={() => SplashScreen.hide()}
            fadeDuration={0}
          >
            {splashMessage.length ? (
              <View style={styles.splashMessage}>
                <TickMessage message={splashMessage} />
              </View>
            ) : null}
            {__DEV__ ? (
              <Button
                onPress={() => this._clearState()}
                title="Clear Persisted State"
              />
            ) : null}
          </ImageBackground>
        </Root>
      );
    }

    return (
      <Root>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={permissionsGranted ? 'Home' : 'AppTracking'}>
            { !permissionsGranted ? (
              <Stack.Screen
                name="AppTracking"
                component={AppTracking}
                options={{ headerShown: false }}
              />
            ) : null}
            <Stack.Screen
              name="Home"
              component={Home}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ArticleView"
              component={ArticleView}
              options={{ header: (props) => <ArticleViewHeader {...props} /> }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Root>
    );
  }

  async _loadAsync() {
    if (__DEV__) {
      // Give an extra 3 seconds to purge the persisted state and restart
      // with a fresh state
      await sleep(3000);
    }

    this.setState({ splashMessage: 'Loading assets' });
    await Font.loadAsync({
      Roboto,
      Roboto_medium: RobotoMedium,
      Arial: Roboto,
      Chomsky,
      ...Ionicons.font,
      ...MaterialCommunityIcons.font
    });
    this.setState({ splashMessage: 'Logging in' });
    await this.props.checkAuth();

    this.setState({
      splashMessage: ''
    });
  }

  async _checkPermissions() {
    let granted = false;
    this.setState({ splashMessage: 'Checking permissions' });

    if (Platform.OS == "ios" && isAvailable()) {
      // We must request tracking permissions on newer iOS versions
      granted = await getTrackingPermissionsAsync();
    } else {
      granted = true;
    }

    this.setState({
      splashMessage: '',
      permissionsGranted: granted,
      isReady: true
    });
  }
}

function mapStateToProps(state) {
  return { isAuthenticating: state.account.isAuthenticating };
}

const RootContainer = connect(mapStateToProps, accountActions)(_RootContainer);

export default class App extends Component {
  state = {
    isSplashReady: false
  };

  render() {
    if (!this.state.isSplashReady) {
      return (
        <AppLoading
          startAsync={this._loadSplashResourcesAsync}
          onFinish={() => {
            this.setState({ isSplashReady: true });
          }}
          onError={console.warn}
          autoHideSplash={false}
        />
      );
    }

    return (
      <Provider store={store}>
        <PersistGate persistor={persistedStore}>
          <RootContainer />
        </PersistGate>
      </Provider>
    );
  }

  // Passed to AppLoading.startAsync; should return a Promise
  async _loadSplashResourcesAsync() {
    const splash = require('../assets/splash.png');
    return Asset.loadAsync(splash);
  }
}
