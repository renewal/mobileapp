/* Implements a simple wrapper for the Renewal API
 *
 * Currently only works with API v1 since that is the only API version.
 */

import axios from 'axios';
import Constants from 'expo-constants';

import { getIdToken } from './auth';
import { sleep } from './utils';

const DEBUG_ARTICLES = [];
const DEBUG_SOURCES = {};
const DEBUG_DATA_SOURCE = {};

if (__DEV__) {
  DEBUG_ARTICLES.push(...require('./data/debug_articles.json'));
  DEBUG_ARTICLES.sort(
    (a, b) => a.article_id - b.article_id);
  Object.assign(DEBUG_SOURCES, require('./data/debug_sources.json'));
  DEBUG_ARTICLES.forEach((a) => (a.site = DEBUG_SOURCES[a.source]));

  function _initDebugData() {
    // Map source data to each articles' site property (which is more in keeping with
    // the current format output by the API)
    const articles = Object.fromEntries(DEBUG_ARTICLES.map((a) => [a.article_id, a]));

    // Split the articles into buckets of 10 each (hard-coded for now)
    const bucketSize = 10;
    const nBuckets = Math.ceil(DEBUG_ARTICLES.length / bucketSize);
    const buckets = [];
    for (let bucket_id = 0; bucket_id < nBuckets; bucket_id++) {
      let bucketArticles = DEBUG_ARTICLES.slice(bucket_id * bucketSize,
                                                (bucket_id + 1) * bucketSize);
      buckets.push({
        bucket_id,
        articles: bucketArticles,
        article_ids: bucketArticles.map(a => a.article_id)
      });
    }

    // Buckets should be returned in order from greatest to least bucket_id
    buckets.reverse();

    Object.assign(DEBUG_DATA_SOURCE, {
      articles,
      buckets,
      bookmarks: []
    });
  }
  _initDebugData();
}

class RenewalAPI {
  constructor(baseURL = Constants.manifest.extra.renewalApi) {
    this.baseURL = baseURL;
    if (!baseURL) {
      if (__DEV__) {
        console.warn(
          'extra.renewalApi not configured in app.config.js; api will use ' +
            'dummy data only'
        );
      } else {
        console.error(
          'extra.renewalApi not configured in app.config.js; it is ' +
            'mandatory for non-development builds'
        );
      }
      this.client = null;
    } else {
      const headers = { Accept: 'application/json' };
      this.client = axios.create({ baseURL, headers });
      // Get the Auth token and add it to the headers
      this.client.interceptors.request.use((config) => {
        return getIdToken().then((token) => {
          config.headers['Authorization'] = `Bearer ${token}`;
          return Promise.resolve(config);
        });
      });
    }
  }

  get articles() {
    return {
      interact: (articleId, params) => {
        // TODO: Rather than have this kind of boilerplate in every API call, I
        // believe axios has an interface for overriding requests, and this
        // would make more sense to implementing mocking during
        // testing/development
        if (this.client === null && __DEV__) {
          return params;
        }
        params = { ...params, timestamp: new Date().toISOString() };
        const url = `/articles/interactions/${articleId}`;
        return this.client
          .post(url, params)
          .then((response) => response.data)
          .catch((error) => {
            console.log(
              `error in article interaction: ${JSON.stringify(error)}`
            );
            return Promise.reject(error);
          });
      }
    };
  }

  async recommendations(params) {
    console.log(`RenewalAPI.recommendations(${JSON.stringify(params)})`);
    if (this.client === null && __DEV__) {
      return this._dummyRecommendations(params);
    }
    return this.client
      .get('/recommendations', { params })
      .then((response) => response.data)
      .catch((error) => {
        console.log(`error fetching recommendations: ${JSON.stringify(error)}`);
        return Promise.reject(error);
      });
  }

  async bookmarks(params) {
    console.log(`RenewalAPI.bookmarks(${JSON.stringify(params)})`);
    // NOTE: In production this would fetch the user's existing bookmarked
    // articles if they aren't already cached in the app.  That isn't
    // implemented yet though (first we need to actually save the user's
    // bookmarks on the backend =)
    if (__DEV__) {
      return this._dummyBookmarks(params);
    }
  }

  // Dummy interfaces for development/test builds; might be nice to move these
  // to a separate class entirely...
  async _dummyRecommendations(params) {
    const { since_id, max_id, limit } = params;
    let buckets = DEBUG_DATA_SOURCE.buckets;

    let start = since_id === undefined ? 0 : since_id;
    let end = max_id === undefined ? buckets.length - 1 : max_id;

    if (end - start > limit) {
      end = start + limit;
    }

    buckets = buckets.slice(start, end);

    // Simulate loading time;
    await sleep(500);

    return buckets;
  }

  async _dummyBookmarks(params) {
    // no-op, any articles that would be bookmarked during development
    // testing would already by loaded by _dummyRecommendations
    return [];
  }
}

const renewalAPI = new RenewalAPI();
export default renewalAPI;
