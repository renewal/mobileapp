import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { Toast } from 'native-base';

import renewalAPI from '../api';
// TODO: Originally this data include the user's saved articles and rejected
// articles in the same data structure; this excludes them for now because
// the dummy data is not user-specific.  We might later include this
// either via a separate API request, or part of the same request with some
// kind of graphql query; for debugging we can load these randomly or according
// to some pattern.

/* Action constants */
const SET_RATING = 'articles/set_rating';
const TOGGLE_BOOKMARKED = 'articles/toggle_bookmarked';
const ARTICLE_INTERACTION = 'articles/article_interaction';
const FETCH = 'articles/fetch';

/* Initial state for articles */
// Most details about articles come from the API, but we also
// set a default rating/bookmarked here
const articleInitialState = {
  rating: 0,
  bookmarked: false
};

const articleListInitialState = {
  list: [],
  current: 0
};

/* NOTE: This data structure is now duplicating some information, because the
 * lists of articles are given both in the "bucket" structures as well as in
 * combined recommendations list.  It's not totally clear to me if there's even
 * any need to store the full bucket structure, but we do for now.
 * Alternatively we store only the buckets, and use a SectionList to display
 * them instead of a FlatList, but it's currently unclear to me how that would
 * affect features like saving the position in the list, so not making any
 * radical changes for now.
 */
export const initialState = {
  articles: {},
  articleBuckets: [],
  articleLists: {
    recommendations: { ...articleListInitialState },
    bookmarks: { ...articleListInitialState }
  }
};

/* Action creators for articles */
const actions = {
  setRating: createAsyncThunk(
    SET_RATING,
    async (arg, { getState, rejectWithValue }) => {
      const { articleId, rating } = arg;
      const articles = getState().articles.articles;
      const prevRating = articles[articleId].prevRating || 0;
      const interaction = { rating, prev_rating: prevRating };
      return await articleInteraction(articleId, interaction, rejectWithValue);
    }
  ),

  toggleBookmarked: createAsyncThunk(
    TOGGLE_BOOKMARKED,
    async (arg, { getState, rejectWithValue }) => {
      const articleId = arg;
      const articles = getState().articles.articles;
      const article = articles[articleId];
      return await articleInteraction(
        articleId,
        { bookmarked: article.bookmarked },
        rejectWithValue
      );
    }
  ),

  // Generic article interaction (e.g. clicked / read, but not rating/bookmark)
  // which does not affect the app state, but which should still be sent as an
  // event to the backend.  We might later decide other article interactions
  // should still be stored in the state to prevent repeats of the same event,
  // but I'm not sure.
  articleInteraction: createAsyncThunk(
    ARTICLE_INTERACTION,
    async (arg, { rejectWithValue }) => {
      const { articleId, interaction } = arg;
      return await articleInteraction(articleId, interaction, rejectWithValue);
    }
  ),

  fetchArticles: createAsyncThunk(FETCH, async (arg, { getState }) => {
    const { listName, old } = arg;
    const state = getState().articles;
    let list = state.articleLists[listName];
    if (list === undefined) {
      list = articleListInitialState;
      state.articleLists[listName] = list;
    }
    const fetchParams = {};
    const fetch = renewalAPI[listName].bind(renewalAPI);

    // NOTE: Originally the data structures and APIs were designed so that
    // there were multiple "article lists", (namely "recommendations" and
    // "bookmarks" though others could easily be added) which are all handled
    // the same way both by the backend API as well as internally.
    // However, recommendations were changed to be returned in a "bucket"
    // structure, so we handle recommendations separately now, which is a
    // bit ugly.
    if (listName === 'recommendations') {
      const buckets = state.articleBuckets;
      if (buckets.length) {
        if (old) {
          fetchParams.max_id = buckets[buckets.length - 1].bucket_id;
        } else {
          fetchParams.since_id = buckets[0].bucket_id;
        }
      }
    } else {
      if (old) {
        fetchParams.max_id = list[list.length - 1];
      } else {
        fetchParams.since_id = list[0];
      }
    }

    return await fetch(fetchParams);
  })
};

async function articleInteraction(articleId, interaction, rejectWithValue) {
  console.log(`article interaction: ${JSON.stringify(interaction)}`);
  try {
    const response = await renewalAPI.articles.interact(articleId, interaction);
    return response;
  } catch (error) {
    return rejectWithValue({ articleId, error: error.message });
  }
}

// Helper for the FETCH reducer; the only difference is how the state is
// updated (articles prepended or appended to the list)
function updateArticles(
  state,
  listName,
  articles,
  articleIds = null,
  old = false
) {
  // TODO: We may want to impose a limit (perhaps a user-configurable setting?)
  // of how many older articles to keep cached
  let list = state.articleLists[listName];
  if (list === undefined) {
    list = articleListInitialState;
    state.articleLists[listName] = list;
  }

  const articlesSet = new Set(list.list);
  const newArticles = [];

  // Now update the appropriate articles array and update the articles
  // object for each article passed to the action
  articles.forEach((article) => {
    state.articles[article.article_id] = { ...articleInitialState };
    Object.assign(state.articles[article.article_id], article);
    if (articleIds === null && !articlesSet.has(article.article_id)) {
      // If a list of articleIds was not provided, just add to the list
      // in the same order the article objects were provided.
      articlesSet.add(article.article_id);
      newArticles.push(article.article_id);
    }
  });

  // If articleIds *is* provided it provides a separate ordering for the
  // articles than the order in the list of article objects (e.g. in the
  // case of loading articles from buckets)
  if (articleIds !== null) {
    articleIds.forEach((articleId) => {
      if (!articlesSet.has(articleId)) {
        articlesSet.add(articleId);
        newArticles.push(articleId);
      }
    });
  }

  if (old) {
    list.list = list.list.concat(newArticles);
  } else {
    list.list = newArticles.concat(list.list);
  }
}

// Common function for toggling an article's bookmarked state
// used in both toggleBookmarked.pending and toggleBookrmarked.rejected
// actions
function toggleBookmarked(articleId, state) {
  const article = state.articles[articleId];
  const bookmarks = state.articleLists.bookmarks.list;
  if (article.bookmarked) {
    // Remove bookmark
    bookmarks.splice(
      bookmarks.findIndex((i) => i === articleId),
      1
    );
  } else {
    // Insert bookmark
    bookmarks.splice(0, 0, articleId);
  }
  article.bookmarked = !article.bookmarked;
}

/* Reducers for article actions */
// NOTE: Per the Redux Toolkit docs
// <https://redux-toolkit.js.org/api/createReducer>, createReducer uses
// immer <https://immerjs.github.io/immer/>, so although it appears we
// are mutating the state rather than returning a new one, this is just
// an illusion.
export const articles = createSlice({
  name: 'articles',
  initialState,
  reducers: {
    setCurrentArticle: (state, action) => {
      const { listName, current } = action.payload;
      state.articleLists[listName].current = current;
    }
  },
  extraReducers: {
    [actions.setRating.pending]: (state, action) => {
      const { articleId, rating } = action.meta.arg;
      const article = state.articles[articleId];
      // Store the previous rating so we can restore it in case the action fails
      article.prevRating = article.rating;
      article.rating = rating;
    },
    [actions.setRating.fulfilled]: (state, action) => {
      const { articleId, rating } = action.meta.arg;
      const article = state.articles[articleId];
      console.log(`successfully set rating ${rating} on article ${articleId}`);
      delete article.prevRating;
    },
    [actions.setRating.rejected]: (state, action) => {
      const { articleId } = action.meta.arg;
      const article = state.articles[articleId];
      if (action.payload) {
        var error = action.payload.error;
      } else {
        error = action.error.message;
      }
      console.log(
        `failed to set rating on article ${articleId}: ${JSON.stringify(error)}`
      );
      article.rating = article.prevRating;
      delete article.prevRating;
    },

    [actions.toggleBookmarked.pending]: (state, action) => {
      const articleId = action.meta.arg;
      toggleBookmarked(articleId, state);
    },
    [actions.toggleBookmarked.fulfilled]: (state, action) => {
      const articleId = action.meta.arg;
      console.log(`successfully toggled bookmarked on article ${articleId}`);
    },
    [actions.toggleBookmarked.rejected]: (state, action) => {
      const articleId = action.meta.arg;
      if (action.payload) {
        var error = JSON.stringify(action.payload.error);
      } else {
        error = action.error.message;
      }
      console.log(
        `failed to toggle bookmarked on article ${articleId}: ${JSON.stringify(
          error
        )}`
      );

      // Set it back to its previous value
      toggleBookmarked(articleId, state);
    },
    [actions.fetchArticles.fulfilled]: (state, action) => {
      const { listName, old } = action.meta.arg;

      // TODO: I wonder if we shouldn't have a separate action for fetchBuckets
      // since it's now really quite different from fetchArticles in general...
      if (listName === 'recommendations') {
        let buckets = action.payload;
        if (!old && buckets.length == 0) {
          Toast.show({
            text: 'No new recommendations; all caught up!',
            textStyle: {textAlign: 'center'},
            position: 'bottom'
          });
          return;
        }
        // The response is a list of buckets
        if (old) {
          state.articleBuckets.concat(buckets);
        } else {
          state.articleBuckets = buckets.concat(state.articleBuckets);
        }

        if (!old) {
          // Reverse the list of buckets so that older buckets are prepended
          // to the articles list *first*
          buckets = buckets.slice().reverse();
        }

        buckets.forEach((bucket) => {
          updateArticles(
            state,
            listName,
            bucket.articles,
            bucket.article_ids,
            old
          );
        });
      } else {
        const articles = action.payload;
        updateArticles(state, listName, articles, null, old);
      }

      // Delete the actual .articles property from each bucket since we don't
      // need it anymore
      state.articleBuckets.forEach((bucket) => {
        delete bucket.articles;
      });
    },
    [actions.fetchArticles.rejected]: (state, action) => {
      const { listName } = action.meta.arg;
      Toast.show({
        text: `failed to get ${listName}: ${action.error.message}`,
        type: 'danger'
      });
    }
  }
});

Object.assign(actions, articles.actions);
export const reducer = articles.reducer;
export default actions;
