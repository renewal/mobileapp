# Renewal

Renewal is react native news app for IOS & Android. This app analyze your
behavior and data for research in association with LRI (computer research
laboratory) : <https://www.lri.fr>

## Try the app

If you just want to try using the app without building it yourself:

### Android

You can try a recent development build of the app by going to
https://expo.io/@renewal-research/projects/renewal, downloading the Expo Go
client, and scanning the QR code at that link.

There is also an internal preview available on the Google Play Store, but
currently only accessible to project members.  A public beta will be made
available soon.

### iOS

Scanning QR codes appears to not work on iOS due to Apple restrictions
(please update this if you find otherwise).  So the best way to try the app
on iOS is to run the latest beta build in TestFlight by following the link
for the open beta test: https://testflight.apple.com/join/hYQIc1XH

Otherwise you can try running the app in development mode as documented
below.


## Development

### Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.

First you'll need to clone the repository :)

```bash
$ git clone git@serveur-gitlab-lisn.lisn.upsaclay.fr:renewal/mobileapp.git
```

To get started building and running the app, you will also need to install
the `expo-cli` application and the Expo Go app on your phone (it is also
possible to run in an emulator, but using a real phone is the easiest to get
started with).  Follow the instructions at
https://docs.expo.io/get-started/installation/

You will also need to install the dependencies, including the development
dependencies.  From the root of this repository run:

```bash
$ npm install --also=dev
```

Then start the expo build server like:

```bash
$ expo start
```

You may see a bunch of warnings like:

```
WARNING: ./app.config.dev.json missing; this file is optional in development
mode but without it some functionality will not work
```

If you want to silence these warnings for now you can run:

```bash
$ echo '{}' > app.config.dev.json
```

Otherwise don't worry about it for now, we'll come back to this.

To run the app you can either scan the displayed QR code in the Expo Go app
on your phone.  Alternatively if you have your Android phone plugged into
your computer via USB and you have [USB debugging
enabled](https://developer.android.com/studio/debug/dev-options#enable) you
can press the 'a' key on your keyboard to launch the app directly.

On MacOS you can run the app in iOS Simulator, which is an optional
component to XCode.  Follow the instructions at
https://docs.expo.dev/workflow/ios-simulator/

If all goes well (and it might not since Expo and React-Native can be very
flaky at times) the app should open on your device, and you will see some
articles load.

You may notice that the loaded articles are a bit old: this is because we
have not configured the app to connect to the backend API yet, in which case
it just loads some sample data.


### Creating the dev config file

The config system for the app allows creating multiple config files for
different deployments of the app (e.g. development, production).  The
default deployment environment is "dev", in which case the builder tries
to load `app.config.dev.json`.  In general the name of the file is
`app.config.<env>.json` where `<env>` is the deployment environment.

In order to test against a real backend API, at a minimum we have to provide
the following in `app.config.dev.json`:

```json
{
  "web": {
    "config": {
      "firebase": {
        "apiKey": "AIzaSyBmLMSaJ82KCZHCYShh-ERSIDX8WyS7Ws0",
        "authDomain": "renewal-dev.firebaseapp.com",
        "databaseURL": "https://renewal-dev.firebaseio.com",
        "projectId": "renewal-dev",
        "storageBucket": "renewal-dev.appspot.com",
        "messagingSenderId": "766454688011",
        "appId": "1:766454688011:web:43fda1f7775c04b31e9a0e",
        "measurementId": "G-0LJ81YF56V"
      }
    }
  },
  "extra": {
    "renewalApi": "https://api.renewal-research.com/v1"
  }
}
```

This provides your app access to the development instance of the Firebase
project for Renewal (despite names like `apiKey` none of this is secret), as
well as provides the app the URL to the Renewal API backend.

If you do not need to make API changes to the backend itself, you can use
the official API server (in the future a development server will be added).

Alternatively, if you are making simultaneous changes to the app and the
backend, see the [backend
documentation](https://gitlab.lisn.upsaclay.fr/renewal/backend/blob/master/README.md)
for instructions on setting up a development server.  Then you can point the
app to your development server by changing `renewalApi` to
`http://localhost:8080/v1`.

Finally, after updating the app configuration, I've found that Expo and/or
the React Native tools tend to behave poorly, so the easiest thing is to
restart the Expo server completely by hitting `Ctrl-C` and re-running:

```bash
$ expo start --android
```

or

```bash
$ expo start --ios
```


### Adding Google authentication support

Google authentication is a bit complicated because there are two different
ways to support it in apps built with Expo and both work differently and
require different configurations:

* The
  [`expo-google-app-auth`](https://docs.expo.io/versions/latest/sdk/google/)
  package provides a way to perform Google authentication through a browser
  window pop-up, that then redirects the auth session back to the app.  It's
  a more jarring user experience, but can work when running the app through
  Expo (and so especially useful during development).  Note: In the latest
  version of Expo this is apparently now deprecated in favor of a new, more
  generic auth package, but we are still on SDK 38 for now.

* The
  [`expo-google-sign-in`](https://docs.expo.io/versions/latest/sdk/google-sign-in/)
  package is the generally preferred method, as it uses your phone's native
  Google account sign-in UI and provides a better overall user experience.
  However, it can *only* be used when the app is built in *standalone* mode.
  That is, it is not run through the Expo Go client.  This is because it
  requires the Google OAuth client ID to built into the app package itself.
  In the case of the Expo Go client, this is not possible since from the
  phone's perspective the app is Expo Go which has its own client ID.

#### Using expo-google-auth-app (for development)

First you will need to create an OAuth client ID.  This needs to be done for
each platform you plan to develop on (Android or iOS currently) by following
the directions at:
https://docs.expo.io/versions/latest/sdk/google/#using-it-inside-of-the-expo-app 

It uses `host.exp.exponent` as the package name/bundle identifier because
this is the identifier for the Expo Go client itself.  When using the
client ID from an app, the app's name has to match the name registered with
the client ID.  When running in the Expo Go client then, we must associate
our cient IDs with the Expo Go app, *not* Renewal itself.  For the
standalone builds (next section) we must do things differently.

Then in your app config file (e.g. `app.config.dev.json`), merge in the
following configuration:

```json
{
  "extra": {
    "auth": {
      "google": {
        "android": {
          "clientID": "XXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.apps.googleusercontent.com"
        },
        "ios": {
          "clientID": "XXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.apps.googleusercontent.com"
        }
      }
    }
  }
}
```

You may omit the `android.clientID` or `ios.clientID` if you are using only
one of those platforms.

Having this configuration in place will enable Google authentication in the
app.


#### Using expo-google-sign-in (for standalone/production builds)

For this case Expo has its own pre-defined sections of the config file
related to Google authentication, because it uses these details when
building the standalone app.

Because we are using Firebase, Firebase provides some of the needed
configuration for us in the form of `google-services.json` (Android)
or `GoogleService-Info.plist` (iOS).

You can download these files through the Firebase Console by following these
instructions: https://docs.expo.io/versions/v38.0.0/sdk/google-sign-in/#usage-with-firebase

See also the general Renewal [development
setup](https://gitlab.lisn.upsaclay.fr/renewal/Renewal#firebase) instructions
regarding setting up the Firebase project and associating apps with the
project.

This requires adding some additional settings to your app configuration file
(e.g. `app.config.dev.json` or `app.config.prod.json`)


## Building standalone/production builds

Running the app through Expo is convenient for development, but for
releasing a build of the app to the app stores it is necessary to produce a
standalone build.

Standalone builds can also be useful for development/testing because some
features (particularly Google authentication) work differently when running
in Expo versus standalone builds.  It is possible for both Android and iOS
to manually install a standalone build, either in an emulator or a physical
device.

### Configuration

Standalone builds will require a slightly different configuration format
from that found in `app.config.dev.json`.  You can create a new config file
named `app.config.prod.json` for production (released to public) builds, or
something like `app.config.dev-standalone.json` for standalone test builds.

In either case the necessary options are similar.  E.g. for a dev standalone
build (using our development Firebase project):

```json
{
  "android": {
    "googleServicesFile": "google-services.json"
  },
  "ios": {
    "config": {
      "googleSignIn": {
        "reservedClientId": "com.googleusercontent.apps.766454688011-ar5k22v21qhjjha3lsphbl0qmq2sfb5b"
      }
    },
    "googleServicesFile": "GoogleService-Info.plist"
  },
  "web": {
    "config": {
      "firebase": {
        "apiKey": "AIzaSyBmLMSaJ82KCZHCYShh-ERSIDX8WyS7Ws0",
        "authDomain": "renewal-dev.firebaseapp.com",
        "databaseURL": "https://renewal-dev.firebaseio.com",
        "projectId": "renewal-dev",
        "storageBucket": "renewal-dev.appspot.com",
        "messagingSenderId": "766454688011",
        "appId": "1:766454688011:web:43fda1f7775c04b31e9a0e",
        "measurementId": "G-0LJ81YF56V"
      }
    }
  },
  "extra": {
    "contactEmail": "erik.bray@lri.fr",
    "renewalApi": "https://api.renewal-research.com/v1"
  }
}
```

Here we need two different "googleServicesFile"s, one for Android and one
for iOS (they contain the same data but in completely different formats).
These files can be downloaded from the Firebase console when setting up the
Firebase project, or at an time afterwards (see [Firebase
setup](https://gitlab.lisn.upsaclay.fr/renewal/Renewal#firebase).

For iOS in particular we also need the config option
`ios.config.googleSignIn.reservedClientId` (see [google-sign-in
configuration](https://docs.expo.dev/versions/v39.0.0/sdk/google-sign-in/#configuration).
This value is read from the `GoogleService-Info.plist` file.  Why we need
this value in two places I don't know, but it's what the Expo documentation
says so we should believe it.  Note that in the `GoogleService-Info.plist`
file this value is called *REVERSED*_CLIENT_ID, whereas the corresponding
key in the Expo config file has the slightly different name
*reserved*ClientId.  I do not know if this is some developer's typo that
became enshrined, or if it's for some reason deliberate.

### Building

To build the project, ensuring that the correct config file is used, we must
pass the `RENEWAL_ENV=<target>` environment variable.  For example
`RENEWAL_ENV=prod` to make a build using the `app.config.prod.json` config.

This is passed to either `expo build:android` or `expo build:ios`.

Either:

```
$ RENEWAL_ENV=prod expo build:android --release-channel=prod --type apk
```

or

```
$ RENEWAL_ENV=prod expo build:ios --release-channel=prod
```

Note that it's also a good idea to pass `--release-channel`.  This organizes
all the Expo builds under a given release channel.  This should be the same
as `RENEWAL_ENV`, typically.

The `RENEWAL_ENV` environment variable is simply read by `app.config.js` to
determine which configuration profile to use.  This was @embray's idea,
though apparently a similar scheme is now being recommended by the Expo
project as well: https://github.com/expo/fyi/blob/master/config-flag-migration.md

The `expo build:<platform>` command will likely ask you some additional
questions, depending on the platform.  In particular for iOS builds you will
have to provide your AppleId and password.  For iOS builds there are two
build types: "archive" and "simulator".  The "simulator" type is necessary
if you want to run the standalone app in the iOS Simulator.
