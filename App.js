/* Polyfill for other missing ES features */
/* This appears to be needed esp. on older iOS versions */
import 'react-app-polyfill/stable';

/* Expo's main loader requires App.js to be at the project root,
 * but that makes things like searching inconvenient since the rest of the source
 * is under src/ so we just keep this as a stub. */
export default from './src/App';
